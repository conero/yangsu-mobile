## Vue3

> 2020.10.13
>
> Joshua Conero

*vue 3 学习笔记*



### 概述

*Vue 被设计为可以自底向上逐层应用。Vue 的核心库只关注视图层。*



#### 模板

指令`v-command`  以 *v-* 开头；模板文本使用 `{{key}}` 类似语法渲染。

 



#### 生命周期

*基本数据模型分析*

```js
// Vue 创建模型过程
const app = Vue.createApp({
    //模型数据
    data(){},
    //方法(模板中所需要的方法列表)
    methods:{
    },
    //实例被创建之后执行代码
    created(){},
    //挂载
    mounted(){},
    //值更新 hook
    updated(){},
    //挂载解除
    unmounted(){},
});

//挂载到 DOM 上
app.mounted('#app');
```

