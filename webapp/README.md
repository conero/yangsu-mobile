## yangsu-mobile

> 基于 vite
>
> Joshua Conero
>
> 2021年7月18日







### 创建项目

- 使用 vite 创建项目： `yarn create vite`
- 安装npm
  - `npm install vue-router@4`                                https://github.com/vuejs/vue-router-next
  - `npm i vite-plugin-style-import -D`    
- 文档地址
  - vueJs                  https://v3.cn.vuejs.org/
  - vue-router        https://next.router.vuejs.org/zh/
  - vant                   https://vant-contrib.gitee.io/vant/v3/#/zh-CN



使用 `deno fmt` 进行格式化：

```shell
deno fmt <dir>
```



项目调试工具

```shell
# 使用 debug 选项来排查。
vite --debug
```

