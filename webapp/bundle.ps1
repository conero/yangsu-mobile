# 依赖附件
# tar
# yarn

# 时间统计
$elapsed = [System.Diagnostics.Stopwatch]::StartNew()

# 日期函数
$wshop = Get-Date -Format "yyMMddHHmmss"
$wshop = "./bundle-$wshop"
$distDir = "./dist"

$name = node ./cmds/version
$name = "wap-v$name"

# 项打包
yarn build

# 代码移动，根据 vite.config base 路径配置
if (Test-Path -Path "./dist/wap") {
    Remove-Item -Recurse ./dist/wap
}
mkdir ./dist/wap/v1
Move-Item ./dist/assets ./dist/wap/v1/assets -Force
write-host "发布模式下，构建的js文件目录已完成。"


# 压缩文件
mkdir $wshop
Set-Location $distDir
tar -zcvf ../bundle/$name.tar.gz *


# 临时文件回收
Set-Location ../
Remove-Item -Recurse $wshop

write-host "Total Elapsed Time: $($elapsed.Elapsed.ToString())"

#停歇10秒
Write-Host '打包完成，窗体将在10秒后退出！'
Start-Sleep 10
