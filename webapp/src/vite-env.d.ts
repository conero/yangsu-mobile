/// <reference types="vite/client" />


// 登录的用户
interface LoginUserInfoTy {
    uid: number
}

// 任意object
interface KvObj {
    [key: string]: any
    [key2: number]: any
}

// api 反馈数据
interface ApiFeedData {
    status: string
    data?: any
    msg: string
}

// 系统信息数据
interface SystemInfo {
    version: string
    release: string
    server: null | {
        author: string,
        code: string,
        email: string,
        version: string,
        release: string,
    }
}

// vue 组件模型
declare module "*.vue" {
    import { DefineComponent } from "vue";
    // eslint-disable-next-line @typescript-eslint/no-explicit-any, @typescript-eslint/ban-types
    const component: DefineComponent<{}, {}, any>;
    export default component;
}

// .env 版本信息
interface ImportMetaEnv {
    readonly VITE_VERSION: string
    readonly VITE_RELEASE: string
}

interface ImportMeta {
    readonly env: ImportMetaEnv
}

declare module 'qs';
declare module 'crypto-js';
declare module 'crypto-js/sha256';
declare module 'crypto-js/aes';