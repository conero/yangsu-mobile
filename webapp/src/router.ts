import { createRouter, createWebHashHistory } from "vue-router";
// index
import Index from "./page/index/App.vue";
import IndexHome from "./page/index/components/Home.vue";
import IndexArticle from "./page/index/components/Article.vue";
import IndexUser from "./page/index/components/User.vue";
import IndexMore from "./page/index/components/More.vue";

// login
import Login from "./page/login/Login.vue";

// finance
import Finance from "./page/finance/Finance.vue";
import FinanceHome from "./page/finance/components/Home.vue";
import FinanceBill from "./page/finance/components/Bill.vue";
import FinanceMaster from "./page/finance/components/Master.vue";

// clan
import Clan from "./page/clan/Clan.vue";
import ClanHome from "./page/clan/components/Home.vue";
import ClanTree from "./page/clan/components/Tree.vue";

// 路由列表
const routes: any = [
  // { "path": "/", component: App },
  {
    "path": "/",
    component: Index,
    children: [ // 嵌套路由
      { path: "/home", component: IndexHome, alias: ["/"] },
      { path: "/article", component: IndexArticle },
      // { path: '/explore', component: IndexArticle },
      { path: "/user", component: IndexUser },
      { path: "/more", component: IndexMore },
    ],
  },
  { "path": "/login", component: Login },
  {
    "path": "/finance",
    component: Finance,
    children: [ // 嵌套路由
      { path: "/finance", component: FinanceHome },
      { path: "/finance/bill", component: FinanceBill },
      { path: "/finance/master", component: FinanceMaster },
    ],
  },
  {
    "path": "/clan",
    component: Clan,
    children: [ // 嵌套路由
      { path: "/clan", component: ClanHome },
      { path: "/clan/:clan_id", component: ClanHome },
      { path: "/clan/tree/:clan_id", component: ClanTree },
    ],
  },
];

// 路由
export default createRouter({
  history: createWebHashHistory(),
  routes,
});
