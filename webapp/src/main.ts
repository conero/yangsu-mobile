import * as Vue from "vue";
import router from "./router";

// https://vant-ui.github.io/vant/#/zh-CN/quickstart#4.-yin-ru-han-shu-zu-jian-de-yang-shi
// Toast 需要手动引入
import 'vant/es/toast/style';
// Dialog
import 'vant/es/dialog/style';

// 5. 创建并挂载根实例
const app = Vue.createApp({});
app.use(router);
app.mount("#app");
