/**
 * Yangsu 项目管理
 */

// 用户中心反馈代码列表
const SquareFeekStatus = {
  unlogin: "10404", //未登录状态
};

class Yangsu {
  /**
       * 获取状态信息
       */
  get status() {
    return SquareFeekStatus;
  }
  /**
       * 通过数据分析用户是否未登录系统
       * @param {*} data
       */
  unloginByData(data: KvObj) {
    if (data && SquareFeekStatus.unlogin === data.status) {
      return true;
    }
    return false;
  }
}

export default Yangsu;
