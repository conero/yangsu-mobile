import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import Components from 'unplugin-vue-components/vite';
import { VantResolver } from '@vant/auto-import-resolver';

// https://vitejs.dev/config/
export default defineConfig(({ command, mode }) => {
  //console.log(command, mode);
  return {
    base: mode === 'production' ? '/wap/v1/' : '/',
    resolve: {
      alias: {
        "vue": "vue/dist/vue.esm-bundler.js", // 定义vue的别名，如果使用其他的插件，可能会用到别名
      }
    },
    server: {
      proxy: {
        "/api": {
          // target: "http://127.0.0.1:9960/api",
          target: "http://127.0.0.1:9960/api",
          changeOrigin: true,
          rewrite: (path) => path.replace(/^\/api/, ""),
        },
        "^/square/data/.*": {
          // target: "http://127.0.0.1:9960/square/data",
          target: "http://127.0.0.1:9960/square/data",
          changeOrigin: true,
          ws: true,
          followRedirects: true,
          rewrite: (path) => path.replace(/^\/square\/data/, ''),
          // configure: (proxy, options) => {
          //   console.log(proxy);
          //   console.log(options);
          // }
        },
        "/images/captcha": {
          target: "http://127.0.0.1:9960/images/captcha",
          changeOrigin: true,
          rewrite: (path) => path.replace(/^\/images\/captcha/, '')
        },
      },
    },
    plugins: [
      vue(),
      Components({
        resolvers: [VantResolver()],
      }),
    ],
  };
});
