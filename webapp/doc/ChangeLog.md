## ChangeLog

> 2020年10月12日

### todo

- [ ] feature) `#231009` 加入“项目管理”板块，以及草稿纸。
- [ ] feature）`#231026` 对wap页面进行配色，使其与web保证一致的海洋风采



### v1.0.1/2023-11-03

> v1 版本优化或实现，暂无非兼容性（破坏性）更新
>
> 服务端版本号：v2.1.2

- **首页**
  - feat: 初步实现首页的动态配色，白天的颜色变化。
  - feat: 首页新增客户端以及服务器版本信息描述固定顶部
- **系统**
  - pref: *bundle.ps1* 打包发布脚本跟进js生成的路径进行自动生成
  - pref: 根据新的的vite初始化脚本对项目配置进行调整（vite `4.5.0`）
  - feat: 新增全局的 `.env` 脚本用于实现，版本信息等获取




### v1.0.0/2023-10-26

> 2021-Plan-Dev
>
> 服务器版本： yangsu/v2.1.1

- break) 项目迁移
  - 由 webpack 构建方式迁移到 vite 的项目构建方式
  - 取消多页面，以单页面APP代替
  - 使用样子自动引入机制减小系统的文件整体尺寸
  - vant、vite 等升级且由原的手动引人调整为按需引入。
  - vite 风格由“选项式 API”迁移为“组合式 API ”。
- fix) 修改迁移中 Toast 提示函数变化引起的错误
- **用户中心**
  - feat: 记住用户登录状态及根据其自动登录实现。
  - pref: 添加用户账号信息！
  - pref: 用户登录验证图片样式美化，用户中心提示语完善等。
- **财务管理**
  - feat: 新增口令表单的写入及显示（<span style="color:red;">NeedToDo </span>查看暂未完成）
  - feat: 账单新增“模板新增”，使其与PC版本保持一致
  - pref: 账单编辑时提交框根据不同模式动态变化，以及操作栏更优化
  
- 多阶段发布
  - v1.0.0-a1/210730
  - v1.0.0-a2/220925   npm依赖更新跨度较大，遂定新版本





### v0.3.0/2021-06-08

> 实际为 `2020-11` 提交的主要代码。由于中途搁浅，本次直接发布原版本计划

- **财务**
  - new) *master* 财务主体添加后台接口，并初步实现数据维护支持
  - optimize) 财务数据维护添加字段 “clan.public_mk”
  - optimize) 财务首页添加财务报表显示部分（<span style="color:red;">NeedToDo</span>）
- **族谱**
  - optimize) 查看族谱族人数据使用安全模式接口代替
  - optimize) 族谱族人数据结果调整仪跟进 `yangsu v1.2.0` 的更变
- **首页**
  - optimize) 完善首页相关的文本描述
- **system**
  - optimize) 前端依赖更新

#### 参考

- [caniuse-lite is outdated](https://www.jianshu.com/p/238d8812c730) `caniuse-lite` 依赖更新。（未能解决，指定提示语 `npx browserslist@latest --update-db`依然无效）



### v0.2.0/2020-10-19

- **首页**
  - optimize) 路由地址优化，删除重复的定义的相同组件；用户界面添加登录退出，以及强化退出同步当前的登录信息
  - +) 添加首页文件列表组件，实现文件列表获取（**Article** 文章组件）
    - 实现文字读取（初步设计）
- **族谱**
  - +) 族人显示列表添加 Tab式成员信息（**Tree**）
    - fixed) 刷新数据后保留旧的值
    - new) 族人列表添加数据搜索，族人详情添加子级列表
    - new) 加入族人编辑表单（新增），实现族人删除、修改等维护
    - optimize) 族谱数据界面重设计，更加集成的页面设计，是进入族谱后，可维护族谱数据以及添加其他相关跳转。
- **财务**
  - new) **财务账单**添加账单详情、新增编辑以及删除等维护操作
- **system**
  - +) 添加windows平台项目打包脚本
  - +) html 静态模板页码（**index.html**）添加页面未全部渲染提示！



### v0.1.1/2020-10-15

- fixed) *族谱、财务列表等数据加载 promise 错误修复*



### v0.1.0/2020-10-15

- **system**
  - new) 项目搭建，使用 `vue3`, `vant` 等库实现
  - new) 添加多页面支持
    - new) 初步布局`login.html` 页面，实现用户登录
    - new）添加布局页面 `clan.html` 族谱
    - new）添加布局页面 `fiance.html` 财务管理
  - new) 添加公共处理库`common/Wap` 使用全局系统调用。
    - +) 实现 Wap 公共的页码处理：首页调整、随机数、全局变量管理等
    - +) 实现 User 通用的用户登录检测，以及获取用户信息登录情况下
    - +) 实现基于 *crypto-jc* 的 Store 数据存储系统
  - new) 添加项目环境变量配置（基于 `vue-cli` 库）
  - optimize) `index.html` 首页演示设计
  - optimize) 用户登录界面添加用户登录检测（公共的），没有登录时统一跳转至首页
  - new) **clan**、**finance** 族人信息获取以及使用list对页面进行布局